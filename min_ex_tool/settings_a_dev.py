# pylint: disable=unused-wildcard-import
import os
from pathlib import Path
from .settings_apps import *

TMP_ENV = dict()
TMP_ENV.update(os.environ)

for key in ["POSTGRES_USER", "POSTGRES_PASSWORD", "RABBITMQ_USER", "RABBITMQ_PASSWORD"]:
    if key + "_FILE" in TMP_ENV:
        with Path(TMP_ENV[key + "_FILE"]).open("r") as f:
            TMP_ENV[key] = f.readline().strip()

DATABASES["default"] = {
    "ENGINE": "django.db.backends.postgresql",
    "NAME": "postgres",
    "USER": TMP_ENV["POSTGRES_USER"],
    "PASSWORD": TMP_ENV["POSTGRES_PASSWORD"],
    "HOST": TMP_ENV["POSTGRES_HOST"],
    "PORT": TMP_ENV["POSTGRES_PORT"],
}

CELERY_BROKER_URL = "amqp://{RABBITMQ_USER}:{RABBITMQ_PASSWORD}@{RABBITMQ_HOST}:{RABBITMQ_PORT}/".format_map(
    TMP_ENV
)
CELERY_ACCEPT_CONTENT = ["pickle", "json", "msgpack", "yaml"]
CELERY_TASK_SERIALIZER = "pickle"
CELERY_RESULT_SERIALIZER = "pickle"
CELERY_RESULT_BACKEND = "django-db"
CELERY_TASK_TIME_LIMIT = 15 * 60
CELERY_WORKER = os.environ["CELERY_WORKER"]

del TMP_ENV
