# pylint: disable=unused-wildcard-import
from .settings_base_django import *

INSTALLED_APPS.append("joke.apps.JokeConfig")
INSTALLED_APPS.append("django_celery_results")
TEMPLATES[0]["DIRS"].append(BASE_DIR / "templates")


STATICFILES_DIRS = [BASE_DIR / "static", BASE_DIR / "static_alt"]
STATIC_ROOT = BASE_DIR / "collected_static"
