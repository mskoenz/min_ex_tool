from django.shortcuts import render
from django.views import generic
from django.urls import reverse
from django.http import HttpResponseRedirect

# Create your views here.
from .models import Joke
from .celery_tasks import capitalize


class OverviewView(generic.ListView):
    template_name = "overview.html"
    context_object_name = "joke_list"

    def get_queryset(self):
        return Joke.objects.order_by("title")


class DetailView(generic.DetailView):
    model = Joke
    template_name = "detail.html"


def create(request):

    if request.POST:
        title = request.POST["title"]
        future = capitalize.delay(title)
        title = future.get(timeout=5)
        joke = Joke.objects.create(title=title, text=request.POST["text"])
        joke.save()
        return HttpResponseRedirect(reverse("joke:detail", args=[joke.id]))
    context = dict()
    return render(request, "create.html", context)
