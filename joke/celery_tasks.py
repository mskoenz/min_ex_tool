#!/usr/bin/env python
# Author:  Mario S. Könz <mskoenz@gmx.net>

from celery import shared_task


@shared_task
def capitalize(text):

    return text.capitalize()
